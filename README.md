# Timestamp Service (FreeCodeCamp)
This is the backend project for FreeCodeCamp - See [Here](https://www.freecodecamp.com/challenges/timestamp-microservice)

## Usage
checkout the [Heroku App](http://timestamp-microserve.herokuapp.com/), It will show you what to do!

## Contributing
1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D

## User Storys
I can pass a string as a parameter, and it will check to see whether that string contains either a unix timestamp or a natural language date (example: January 1, 2016).


If it does, it returns both the Unix timestamp and the natural language form of that date.


If it does not contain a date or Unix timestamp, it returns null for those properties.

## License
Released under GNU (General Public License)
Permission for individuals, Organizations and Companies to run, study, share (copy), and modify the software and its source.
Copyright Adam Kelly 2016-2017

