// Requires
var express = require('express');
var moment = require('moment');
var path = require('path');

// Initialisse Express App
var app = express();

// Variables
var port = process.env.PORT || 3500;
var obj = {
    "unix": 0,
    "natural": "January 1, 1970"
};

// Serve our Index Page 
app.get('/', function(req, res) {
  var fileName = path.join(__dirname, 'index.html');
  
  // Sends the file
  res.sendFile(fileName, function (err) {
      
    // Handle Error
    if (err) {
      console.log(err);
      res.status(err.status).end();
    }
    else {
      console.log('Sent:', fileName);
    }
  });
});

// Our Api
app.get('/:theUrl', function(req, res) {

    // Sets the url variable by taking away the forward slash
    var url = req.url.substr(1);

    // If the url is a number, Change the format to readable
    if (!isNaN(url)) {

        // Variables
        var unixTimestamp = +url;

        // Changes our JSON Object
        obj.unix = moment.unix(unixTimestamp).unix();
        obj.natural = moment.unix(unixTimestamp).format('MMMM D, YYYY');

        // Error Handling
        handleNull();
        
        // Returns our JSON Object
        res.json(obj);
    }

    // Else, Change it to a unix timestamp
    else {
        // Turns Our URL to Intended Format
        var decodedURL = decodeURIComponent(url);
        obj.unix = moment(decodedURL, "MMMM D, YYYY").unix();
        obj.natural = moment(decodedURL, "MMMM D, YYYY").format('MMMM D, YYYY');
        
        // Error Handling
        handleNull();

        // Returns our JSON Object
        res.json(obj);
    }

});

// Print to console the port that is being listened to
app.listen(port, function() {
    console.log("Listening on port " + port);
});

// Handle our errors
function handleNull(){
    if (obj.natural === "Invalid date") {
        obj.natural = "null";
    }
    else {
        obj.natural = obj.natural;
    }
}